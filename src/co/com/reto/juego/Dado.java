package co.com.reto.juego;

import java.util.List;
import java.util.Random;

public class Dado {

    private Dado dado;
    static int recorre;

    public Dado(Dado dado) {
        this.dado = dado;
    }

    public Dado getDado() {
        return dado;
    }

    public void setDado(Dado dado) {
        this.dado = dado;
    }


    public static void lanzarDado() {

         int lado = (int) (Math.random()*7);

         if (lado == 0){
             lado += 1 ;

         }
         recorre = lado * 100;

    }
}

